@if(Session::has('message'))
    <div class="alert alert-success rounded">
        {{ Session::get('message') }}
    </div>
@endif
