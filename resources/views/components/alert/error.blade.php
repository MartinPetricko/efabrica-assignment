@if($errors->any())
    <div class="alert alert-danger rounded">
        {{ $errors->first() }}
    </div>
@endif
