<x-layouts.app>
    <a class="bg-blue-500 px-8 py-4 text-lg font-bold rounded flex text-white w-fit mb-12" href="{{ route('web.employees.index') }}">Employees</a>

    <x-alert.error></x-alert.error>
    <x-alert.success></x-alert.success>

    <form class="flex flex-col gap-6 max-w-lg" action="{{ route('web.employees.store') }}" method="post">
        @csrf

        <input class="border px-6 py-3 rounded" type="text" name="name" placeholder="Name" value="{{ old('name') }}">
        <x-form.error name="name"></x-form.error>

        <input class="border px-6 py-3 rounded" type="number" min="0" name="age" placeholder="Age" value="{{ old('age') }}">
        <x-form.error name="age"></x-form.error>

        <select class="border px-6 py-3 rounded" name="sex" value="{{ old('sex') }}">
            <option>Male</option>
            <option>Female</option>
            <option>Other</option>
        </select>
        <x-form.error name="sex"></x-form.error>

        <button class="bg-blue-500 px-8 py-4 text-lg font-bold rounded flex text-white justify-center mb-12" type="submit">Create</button>
    </form>
</x-layouts.app>
