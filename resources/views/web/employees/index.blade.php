<x-layouts.app>
    <a class="bg-blue-500 px-8 py-4 text-lg font-bold rounded flex text-white w-fit mb-12" href="{{ route('web.employees.create') }}">Create Employee</a>

    <x-alert.error></x-alert.error>
    <x-alert.success></x-alert.success>

    <table class="w-full border">
        <thead>
            <tr>
                <th class="text-left px-4 py-2">#</th>
                <th class="text-left px-4 py-2">Name</th>
                <th class="text-left px-4 py-2">Sex</th>
                <th class="text-left px-4 py-2">Age</th>
                <th class="text-left px-4 py-2"></th>
            </tr>
        </thead>

        <tbody>
            @foreach($employees as $employee)
                <tr>
                    <td class="px-4 py-2 w-1">{{ $employee->id }}</td>
                    <td class="px-4 py-2">{{ $employee->name }}</td>
                    <td class="px-4 py-2">{{ $employee->sex }}</td>
                    <td class="px-4 py-2">{{ $employee->age }}</td>
                    <td class="px-4 py-2 w-1">
                        <div class="flex items-center gap-6 w-fit">
                            <a href="{{ route('web.employees.show', ['employee' => $employee]) }}">show</a>
                            <a href="{{ route('web.employees.edit', ['employee' => $employee]) }}">edit</a>
                            <form action="{{ route('web.employees.destroy', ['employee' => $employee]) }}" method="post">
                                @csrf
                                @method('DELETE')

                                <button class="flex items-center justify-center w-10 h-10 rounded-full bg-red-500 text-white font-bold" type="submit">X</button>
                            </form>
                        </div>
                    </td>
                </tr>
            @endforeach
        </tbody>

        <tfoot>
            <tr>
                <th class="text-left px-4 py-2">#</th>
                <th class="text-left px-4 py-2">Name</th>
                <th class="text-left px-4 py-2">Sex</th>
                <th class="text-left px-4 py-2">Age</th>
                <th class="text-left px-4 py-2"></th>
            </tr>
        </tfoot>
    </table>

    <div class="h-96">
        <canvas id="chart" width="400" height="400"></canvas>
    </div>

    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.0/dist/chart.min.js"></script>

        <script>
            let data = @json($age_statistics)

                let
            ctx = document.getElementById('chart').getContext('2d')

            let myChart = new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: Object.keys(data),
                    datasets: [{
                        label: 'Age',
                        data: Object.values(data),
                        borderWidth: 1,
                        backgroundColor: 'rgb(59, 130, 246)',
                    }],
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true,
                        },
                    },
                    responsive: true,
                    maintainAspectRatio: false,
                },
            })
        </script>
    @endpush

</x-layouts.app>
