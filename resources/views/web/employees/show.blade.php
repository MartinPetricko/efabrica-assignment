<x-layouts.app>
    <a class="bg-blue-500 px-8 py-4 text-lg font-bold rounded flex text-white w-fit mb-12" href="{{ route('web.employees.index') }}">Employees</a>

    <x-alert.error></x-alert.error>
    <x-alert.success></x-alert.success>

    <div class="flex flex-col gap-6 max-w-lg">
        <input class="border px-6 py-3 rounded" type="text" name="name" placeholder="Name" value="{{ $employee->name }}" disabled>

        <input class="border px-6 py-3 rounded" type="number" min="0" name="age" placeholder="Age" value="{{ $employee->age }}" disabled>

        <select class="border px-6 py-3 rounded" name="sex" value="{{ $employee->sex }}" disabled>
            <option>Male</option>
            <option>Female</option>
            <option>Other</option>
        </select>
    </div>
</x-layouts.app>
