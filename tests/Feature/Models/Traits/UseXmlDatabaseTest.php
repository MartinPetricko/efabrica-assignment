<?php

namespace Tests\Feature\Models\Traits;

use Tests\TestCase;
use SimpleXMLElement;
use App\Models\Employee;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Config;

class UseXmlDatabaseTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Config::set('database.connections.xml.path', storage_path('tests/xml'));
    }

    public function tearDown(): void
    {
        File::deleteDirectory(storage_path('tests'));

        parent::tearDown();
    }

    public function test_xml_is_created(): void
    {
        Employee::create([
            'name' => 'John',
        ]);

        $this->assertFileExists(config('database.connections.xml.path') . '/employees.xml');
    }

    public function test_record_can_be_created(): void
    {
        Employee::create([
            'name' => 'John',
        ]);

        $xml = new SimpleXMLElement(config('database.connections.xml.path') . '/employees.xml', 0, true);

        $this->assertCount(1, $xml->xpath('/employees'));
    }

    public function test_record_can_be_retrieved(): void
    {
        Employee::create([
            'name' => 'John',
        ]);

        Employee::create([
            'name' => 'Doe',
        ]);

        $this->assertCount(2, Employee::get());
    }

    public function test_record_can_be_found(): void
    {
        Employee::create([
            'name' => 'John',
        ]);

        Employee::create([
            'name' => 'Doe',
        ]);

        $john = Employee::find(1);
        $doe  = Employee::find(2);

        $this->assertEquals('John', $john->name);
        $this->assertEquals('Doe', $doe->name);
    }

    public function test_record_can_be_deleted(): void
    {
        Employee::create([
            'name' => 'John',
        ]);

        Employee::create([
            'name' => 'Doe',
        ]);

        $john = Employee::find(1);

        $john->delete();

        $this->assertCount(1, Employee::get());
    }

    public function test_record_can_be_updated(): void
    {
        Employee::create([
            'name' => 'John',
        ]);

        $employee = Employee::find(1);

        $employee->name = 'Doe';

        $employee->save();

        $this->assertEquals('Doe', Employee::find(1)->name);

        $this->assertCount(1, Employee::get());
    }
}
