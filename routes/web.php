<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Web\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [EmployeeController::class, 'index']);
Route::get('/employees', [EmployeeController::class, 'index'])->name('web.employees.index');
Route::get('/employees/create', [EmployeeController::class, 'create'])->name('web.employees.create');
Route::post('/employees/create', [EmployeeController::class, 'store'])->name('web.employees.store');
Route::get('/employees/{employee}', [EmployeeController::class, 'show'])->name('web.employees.show');
Route::get('/employees/{employee}/edit', [EmployeeController::class, 'edit'])->name('web.employees.edit');
Route::put('/employees/{employee}/edit', [EmployeeController::class, 'update'])->name('web.employees.update');
Route::delete('/employees/{employee}/delete', [EmployeeController::class, 'destroy'])->name('web.employees.destroy');
