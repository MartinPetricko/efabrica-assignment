<?php

namespace App\Actions\Web\Employee;

use App\Models\Employee;
use App\Services\Employee\EmployeeServiceInterface;
use App\Http\Requests\Web\Employee\StoreEmployeeRequest;

class StoreEmployeeAction
{
    private EmployeeServiceInterface $employeeService;

    public function __construct(EmployeeServiceInterface $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function handle(StoreEmployeeRequest $request): Employee
    {
        $employee = $this->employeeService->create();

        $employee->fill($request->validated());

        $this->employeeService->save($employee);

        return $employee;
    }
}
