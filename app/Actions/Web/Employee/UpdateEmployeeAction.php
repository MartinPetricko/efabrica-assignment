<?php

namespace App\Actions\Web\Employee;

use App\Models\Employee;
use App\Services\Employee\EmployeeServiceInterface;
use App\Http\Requests\Web\Employee\UpdateEmployeeRequest;

class UpdateEmployeeAction
{
    private EmployeeServiceInterface $employeeService;

    public function __construct(EmployeeServiceInterface $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function handle(Employee $employee, UpdateEmployeeRequest $request): Employee
    {
        $employee->fill($request->validated());

        $this->employeeService->save($employee);

        return $employee;
    }
}
