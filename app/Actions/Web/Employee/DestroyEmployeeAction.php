<?php

namespace App\Actions\Web\Employee;

use App\Models\Employee;
use App\Services\Employee\EmployeeServiceInterface;

class DestroyEmployeeAction
{
    private EmployeeServiceInterface $employeeService;

    public function __construct(EmployeeServiceInterface $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    public function handle(Employee $employee): void
    {
        $this->employeeService->delete($employee);
    }
}
