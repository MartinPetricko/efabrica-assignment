<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Locale;

class SetLocaleAcceptLanguage
{
    public function handle(Request $request, Closure $next): mixed
    {
        app()->setLocale($this->getAcceptedLocale($request));

        return $next($request);
    }

    protected function getAcceptedLocale(Request $request): string
    {
        $available = Cache::rememberForever('resources.languages', function () {
            return array_diff(scandir(resource_path('lang')), ['..', '.']);
        });

        $preferences = array_map(static function ($locale) {
            return str_replace('_', '-', $locale);
        }, $request->getLanguages());

        foreach ($preferences as $preference) {
            if (in_array($preference, $available, true)) {
                return $preference;
            }
        }

        $user = $request->user();

        if ($user && !empty($user->locale)) {
            array_unshift($preferences, $user->locale);
        }

        reset($preferences);

        foreach ($preferences as $preference) {
            $preference = Locale::lookup($available, $preference);

            if (!empty($preference)) {
                return $preference;
            }
        }

        return config('app.locale', 'en');
    }
}
