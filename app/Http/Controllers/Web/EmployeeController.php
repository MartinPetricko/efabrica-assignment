<?php

namespace App\Http\Controllers\Web;

use App\Models\Employee;
use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use App\Actions\Web\Employee\StoreEmployeeAction;
use App\Actions\Web\Employee\UpdateEmployeeAction;
use App\Actions\Web\Employee\DestroyEmployeeAction;
use App\Http\Requests\Web\Employee\StoreEmployeeRequest;
use App\Http\Requests\Web\Employee\UpdateEmployeeRequest;
use App\Repositories\Employee\EmployeeRepositoryInterface;

class EmployeeController extends Controller
{
    private EmployeeRepositoryInterface $employeeRepository;

    public function __construct(EmployeeRepositoryInterface $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    public function index(): View
    {
        return view('web.employees.index', [
            'employees'      => $this->employeeRepository->all(),
            'age_statistics' => $this->employeeRepository->getAgeStatistics(),
        ]);
    }

    public function create(): View
    {
        return view('web.employees.create');
    }

    public function store(StoreEmployeeRequest $request, StoreEmployeeAction $action): RedirectResponse
    {
        $employee = $action->handle($request);

        return redirect()->route('web.employees.show', ['employee' => $employee])->with([
            'message' => __('Employee created successfuly')
        ]);
    }

    public function show(Employee $employee): View
    {
        return view('web.employees.show', [
            'employee' => $employee,
        ]);
    }

    public function edit(Employee $employee): View
    {
        return view('web.employees.edit', [
            'employee' => $employee,
        ]);
    }

    public function update(UpdateEmployeeRequest $request, UpdateEmployeeAction $action, Employee $employee): RedirectResponse
    {
        $employee = $action->handle($employee, $request);

        return redirect()->route('web.employees.show', ['employee' => $employee])->with([
            'message' => __('Employee updated successfuly')
        ]);
    }

    public function destroy(DestroyEmployeeAction $action, Employee $employee): RedirectResponse
    {
        $action->handle($employee);

        return redirect()->route('web.employees.index')->with([
            'message' => __('Employee removed successfuly')
        ]);
    }
}
