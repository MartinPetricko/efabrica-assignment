<?php

namespace App\Http\Requests\Web\Employee;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEmployeeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'age'  => ['required', 'numeric', 'min:0'],
            'sex'  => ['required', 'string', 'max:255'],
        ];
    }
}
