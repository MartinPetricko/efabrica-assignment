<?php

namespace App\Providers;

use App\Services\EloquentService;
use Illuminate\Support\ServiceProvider;
use App\Repositories\EloquentRepository;
use App\Services\EloquentServiceInterface;
use App\Services\Employee\EmployeeService;
use App\Repositories\EloquentRepositoryInterface;
use App\Services\Employee\EmployeeServiceInterface;
use App\Repositories\Employee\EmployeeCacheRepository;
use App\Repositories\Employee\EmployeeRepositoryInterface;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->bind(EloquentRepositoryInterface::class, EloquentRepository::class);
        $this->app->bind(EloquentServiceInterface::class, EloquentService::class);

        $this->app->bind(EmployeeRepositoryInterface::class, EmployeeCacheRepository::class);
        $this->app->bind(EmployeeServiceInterface::class, EmployeeService::class);
    }

    public function boot(): void
    {
        //
    }
}
