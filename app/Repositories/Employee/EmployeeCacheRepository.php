<?php

namespace App\Repositories\Employee;

use App\Repositories\HasBasicCacheMethods;

class EmployeeCacheRepository extends EmployeeRepository
{
    use HasBasicCacheMethods;
}
