<?php

namespace App\Repositories\Employee;

use Closure;
use App\Models\Employee;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Expression;
use App\Repositories\EloquentRepositoryInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface EmployeeRepositoryInterface extends EloquentRepositoryInterface
{
    public function getModel(): Model|Employee;

    public function all(): Collection|Model|Employee;

    public function where(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model|Employee;

    public function get(array $filter = null): LengthAwarePaginator|Collection|Model|Employee;

    public function find(mixed $id): Model|Employee|null;

    public function findOrFail(mixed $id): Model|Employee;

    public function first(): Model|Employee|null;

    public function firstOrFail(): Model|Employee;

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Employee|null;

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model|Employee;

    public function save(Model|Employee $model): Model|Employee;

    public function delete(Model|Employee $model): void;

    public function restore(Model|Employee $model): void;

    public function forceDelete(Model|Employee $model): void;

    public function getAgeStatistics(): Collection;
}
