<?php

namespace App\Repositories\Employee;

use Closure;
use App\Models\Employee;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\EloquentRepository;
use Illuminate\Database\Query\Expression;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class EmployeeRepository extends EloquentRepository implements EmployeeRepositoryInterface
{
    protected Model|Employee $model;

    public function __construct(Employee $model)
    {
        parent::__construct($model);
    }

    public function all(): Collection
    {
        return $this->model::get();
    }

    public function where(string|array|Expression|Closure $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Collection|Model
    {
        return $this->model::get()->where($column, $operator, $value, $boolean);
    }

    public function get(array $filter = null): LengthAwarePaginator|Collection
    {
        return $this->model::get();
    }

    public function findOrFail(mixed $id): Model
    {
        $model = $this->model::find($id);

        if (!$model) {
            throw new ModelNotFoundException();
        }

        return $model;
    }

    public function first(): ?Model
    {
        return $this->model::get()->first();
    }

    public function firstOrFail(): Model
    {
        return $this->model::get()->firstOrFail();
    }

    public function firstWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): ?Model
    {
        return $this->model::get()->firstWhere($column, $operator, $value, $boolean);
    }

    public function firstOrFailWhere(string|array|Closure|Expression $column, mixed $operator = null, mixed $value = null, string $boolean = 'and'): Model
    {
        return $this->model::get()->where($column, $operator, $value, $boolean)->firstOrFail();
    }

    public function getAgeStatistics(): Collection
    {
        $employees = $this->all();

        $ages = $employees->map(fn($employee) => ['age' => $employee->age])
                          ->groupBy('age')
                          ->map(function ($age) {
                              return $age->count();
                          });

        return $ages;
    }
}
