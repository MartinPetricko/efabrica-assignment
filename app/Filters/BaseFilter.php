<?php

namespace App\Filters;

use EloquentFilter\ModelFilter;

class BaseFilter extends ModelFilter
{
    public function sortBy($orderBy): BaseFilter
    {
        //if (method_exists($this->getModel(), 'isTranslationAttribute') && $this->getModel()->isTranslationAttribute($orderBy)) {
        //    return $this->orderByTranslation($orderBy, $this->input('sortDirection') ?? 'asc');
        //}

        return $this->orderBy($orderBy, $this->input('sortDirection') ?? 'asc');
    }
}
