<?php

namespace App\Models\Traits;

use Exception;
use SimpleXMLElement;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;

trait UseXmlDatabase
{
    protected static string $filePath;

    protected static string $fileName;

    protected static string $root;

    protected static string $child;

    private static function init(): void
    {
        self::$child    = Str::lower(class_basename(new self()));
        self::$root     = Str::plural(self::$child);
        self::$fileName = self::$root . '.xml';
        self::$filePath = config('database.connections.xml.path') . "/" . self::$fileName;
    }

    public static function find(mixed $id): ?self
    {
        $xml = self::getXml();

        $model = new self();

        $results = $xml->xpath("/" . self::$root . "/" . self::$child . "[$model->primaryKey=\"$id\"]");

        if (!$results) {
            return null;
        }

        foreach ($results[0] as $key => $value) {
            $model->$key = (string)$value;
        }

        $model->exists = true;

        return $model;
    }

    public static function get(): Collection
    {
        $xml = self::getXml();

        $results = $xml->xpath("/" . self::$root . "/" . self::$child);

        $models = collect();

        foreach ($results as $result) {
            $model = new self();

            foreach ($result as $key => $value) {
                $model->$key = (string)$value;
            }

            $model->exists = true;

            $models->push($model);
        }

        return $models;
    }

    public function save(array $options = []): bool
    {
        $xml = self::getXml();

        if ($this->exists) {
            $child = $xml->xpath("/" . self::$root . "/" . self::$child . "[$this->primaryKey=\"" . $this->{$this->primaryKey} . "\"]")[0];
        } else {
            $child = $xml->addChild(self::$child);
        }

        if ($child === null) {
            return false;
        }

        foreach ($this->attributes as $key => $value) {
            $child->$key = $value;
        }

        if(!$this->{$this->primaryKey}) {
            if ($this->incrementing) {
                $ids = array_map('intval', $xml->xpath("/" . self::$root . "/" . self::$child . "/" . $this->primaryKey));

                $ids[] = 0;

                $this->{$this->primaryKey} = max($ids) + 1;

                $child->{$this->primaryKey} = $this->{$this->primaryKey};
            } else {
                throw new Exception("Primary key $this->primaryKey not set");
            }
        }

        $result = $xml->asXML(self::$filePath);

        if ($result === false) {
            throw new Exception("Cannot save values into " . self::$filePath);
        }

        return true;
    }

    public static function create(array $attributes): Model
    {
        $model = new self();

        $model->fill($attributes);

        $model->wasRecentlyCreated = true;

        if ($model->save()) {
            $model->exists = true;
        }

        return $model;
    }

    public function delete(): bool
    {
        $xml = self::getXml();

        if (!$this->exists) {
            return true;
        }

        $result = $xml->xpath("/" . self::$root . "/" . self::$child . "[$this->primaryKey=\"" . $this->{$this->primaryKey} . "\"]");

        $dom = dom_import_simplexml($result[0]);
        $dom->parentNode->removeChild($dom);

        $result = $xml->asXML(self::$filePath);

        if ($result === false) {
            throw new Exception("Cannot save values into " . self::$filePath);
        }

        return true;
    }

    private static function getXml(): SimpleXMLElement
    {
        self::init();

        if (!file_exists(self::$filePath)) {
            File::makeDirectory(config('database.connections.xml.path'), recursive: true, force: true);

            File::put(self::$filePath, "<" . self::$root . "/>");
        }

        return new SimpleXMLElement(self::$filePath, 0, true);
    }
}
