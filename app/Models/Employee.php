<?php

namespace App\Models;

use App\Models\Traits\UseXmlDatabase;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Repositories\Employee\EmployeeRepositoryInterface;

class Employee extends Model
{
    use HasFactory, UseXmlDatabase;

    protected $fillable = [
        'name',
        'sex',
        'age',
    ];

    public function resolveRouteBinding($value, $field = null): self
    {
        if (!$field || $field === $this->primaryKey) {
            return app(EmployeeRepositoryInterface::class)->findOrFail($value);
        }

        return app(EmployeeRepositoryInterface::class)->firstOrFailWhere($field, $value);
    }
}
