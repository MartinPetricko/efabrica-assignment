<?php

namespace App\Services\Employee;

use App\Models\Employee;
use App\Services\EloquentServiceInterface;
use Illuminate\Database\Eloquent\Model;

interface EmployeeServiceInterface extends EloquentServiceInterface
{
    public function create(): Model|Employee;

    public function save(Model|Employee $model): Model|Employee;

    public function delete(Model|Employee $model): void;

    public function restore(Model|Employee $model): void;

    public function forceDelete(Model|Employee $model): void;
}
