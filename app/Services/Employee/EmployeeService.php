<?php

namespace App\Services\Employee;

use App\Repositories\Employee\EmployeeRepositoryInterface;
use App\Services\EloquentService;

class EmployeeService extends EloquentService implements EmployeeServiceInterface
{
    public function __construct(EmployeeRepositoryInterface $repository)
    {
        parent::__construct($repository);
    }
}
